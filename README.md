#Post Office

##Overview
Post Office is an email framework for Go.

##Installation
To install Post Office
```
$ go get bitbucket.org/mreisch/postoffice
```
##Usage
To use Post Office you must create an instance of Post Office. Depending on the environment you want, you can set its variables
for Mock and SMTP mailing.

To create a Mock Mailer and send a message:

```
import (
    "bitbucket.org/mreisch/postoffice"
	"bytes"
	"strings"
	"testing"
)
mailServer := &bytes.Buffer{}
	po := postoffice.New()
	po.Mock=true
	po.Writer= mailServer
	err:= po.Init()
	err = po.SendMail(postoffice.Email{
    		To:       []string{"some@email.com", "abc@def.com"},
    		ToNames:  []string{"John", "Bill"},
    		From:     "some@guy.com",
    		FromName: "Sven",
    		ReplyTo:  "an@email.com",
    		Subject:  "Email!",
    		TextBody: "No html here",
    		HTMLBody: "<html>body</html>",
    	})
```

To create an SMTP Mailer and send a message follow the same example except set Mock to false and include mail server and
 Auth information in their respective fields.
