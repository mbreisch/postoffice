package postoffice
import (
	"io"
	"net/smtp"
	"errors"
	"strings"
	"text/template"
	"bytes"
	"fmt"
)


type PostOffice struct {
	// The mailer type either mock or smtp
	Mailer Mailer
	//Mock creates a mock mailer which logs emails if true
	Mock bool
	//Where the mock mailer writes to
	Writer io.Writer
	//The desired smtp server
	Server string
	//Your authentication method
	Authentication smtp.Auth
}
//PostOffice Constructor creates a default mock mailer to use. Still requires that a Writer be specified.
func New() *PostOffice{
	po := &PostOffice{Mock:true}
	return po
}

func (po *PostOffice)SendMail(data Email) error{
	return po.Mailer.send(data)
}

type Mailer interface {
	send(Email)error
}

type Email struct {
	To, Cc, Bcc                []string
	ToNames, CcNames, BccNames []string
	FromName, From             string
	ReplyToName, ReplyTo       string
	Subject                    string

	TextBody string
	HTMLBody string
}
type logMailer struct {
	io.Writer
}

func (l logMailer) send(data Email) error {
	buf := &bytes.Buffer{}
	err := emailTmpl.Execute(buf, data)
	if err != nil {
		return err
	}

	toSend := bytes.Replace(buf.Bytes(), []byte{'\n'}, []byte{'\r', '\n'}, -1)

	_, err = l.Write(toSend)
	return err
}

type smtpMailer struct {
	Server string
	Auth   smtp.Auth
}
func (s smtpMailer) send(data Email) error {
	buf := &bytes.Buffer{}
	err := emailTmpl.Execute(buf, data)
	if err != nil {
		return err
	}

	toSend := bytes.Replace(buf.Bytes(), []byte{'\n'}, []byte{'\r', '\n'}, -1)

	return smtp.SendMail(s.Server, s.Auth, data.From, data.To, toSend)
}

//Init creates an instance of a mailer
func (po *PostOffice)Init()error{
	var err error
	if po.Mock{
		if po.Writer == nil {
			err=errors.New("PostOffice set to mock mode but Writer not specified.")
		}else {
			po.Mailer =logMailer{po.Writer}
		}
	}else {
		if len(po.Server) == 0 || po.Authentication == nil {
			err = errors.New("Server information missing! Please check that server and authentication information are set")
		}else {
			po.Mailer=smtpMailer{po.Server, po.Authentication}
		}
	}
	return err
}

func namedAddress(name, address string) string {
	if len(name) == 0 {
		return address
	}

	return fmt.Sprintf("%s <%s>", name, address)
}

func namedAddresses(names, addresses []string) string {
	if len(names) == 0 {
		return strings.Join(addresses, ", ")
	}

	buf := &bytes.Buffer{}
	first := true

	for i, address := range addresses {
		if first {
			first = false
		} else {
			buf.WriteString(", ")
		}

		buf.WriteString(namedAddress(names[i], address))
	}

	return buf.String()
}
var emailTmpl = template.Must(template.New("email").Funcs(template.FuncMap{
	"join":           strings.Join,
	"namedAddress":   namedAddress,
	"namedAddresses": namedAddresses,
}).Parse(`To: {{namedAddresses .ToNames .To}}{{if .Cc}}
Cc: {{namedAddresses .CcNames .Cc}}{{end}}{{if .Bcc}}
Bcc: {{namedAddresses .BccNames .Bcc}}{{end}}
From: {{namedAddress .FromName .From}}
Subject: {{.Subject}}{{if .ReplyTo}}
Reply-To: {{namedAddress .ReplyToName .ReplyTo}}{{end}}
MIME-Version: 1.0
Content-Type: multipart/alternative; boundary="===============284fad24nao8f4na284f2n4=="
Content-Transfer-Encoding: 7bit
--===============284fad24nao8f4na284f2n4==
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 7bit
{{.TextBody}}
--===============284fad24nao8f4na284f2n4==
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: 7bit
{{.HTMLBody}}
--===============284fad24nao8f4na284f2n4==--
`))