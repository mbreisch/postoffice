package postoffice

import (
	"bytes"
	"strings"
	"testing"
)

func TestLogMailer(t *testing.T){
	mailServer := &bytes.Buffer{}
	po := New()
	po.Mock=true
	po.Writer= mailServer
	err:= po.Init()
	if err != nil{
		t.Error(err)
	}

	err = po.SendMail(Email{
		To:       []string{"some@email.com", "a@a.com"},
		ToNames:  []string{"Jake", "Noname"},
		From:     "some@guy.com",
		FromName: "Joseph",
		ReplyTo:  "an@email.com",
		Subject:  "Email!",
		TextBody: "No html here",
		HTMLBody: "<html>body</html>",
	})

	if err != nil {
		t.Error(err)
	}
	if mailServer.Len() == 0 {
		t.Error("It should have logged the e-mail.")
	}

	str := mailServer.String()
	if !strings.Contains(str, "From: Joseph <some@guy.com>") {
		t.Error("From line not present.")
	}

	if !strings.Contains(str, "To: Jake <some@email.com>, Noname <a@a.com>") {
		t.Error("To line not present.")
	}

	if !strings.Contains(str, "No html here") {
		t.Error("Text body not present.")
	}

	if !strings.Contains(str, "<html>body</html>") {
		t.Error("Html body not present.")
	}


}
func TestSMTPMailer(t *testing.T) {
	po := New()
	po.Mock=false
	po.Server="server"
	po.Authentication=nil
	err:= po.Init()
	if err == nil{
		t.Error("It should have produced an error %s ")
	}


}